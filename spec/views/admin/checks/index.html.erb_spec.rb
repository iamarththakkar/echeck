require 'rails_helper'

RSpec.describe "admin/checks", type: :view do
  before(:each) do
    assign(:checks, [
    	Check.create(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20)),
    	Check.create(date: "2017/04/31", memo1: "For Second payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20))
    ])
  end

  # it "displays all the checks" do
  #   render
  #   expect(rendered).to match /For first payment/
  #   expect(rendered).to match /For Second payment/
  # end
end
