require "rails_helper"

RSpec.describe Admin::ChecksController, type: :routing do
  describe "routing" do
    it "routes to index page" do
      expect(:get => "/admin/checks").to route_to("admin/checks#index")
    end

    it "routes to new page" do
      expect(:get => "/admin/checks/new").to route_to("admin/checks#new")
    end

    it "routes to create page" do
      expect(:post => "/admin/checks").to route_to("admin/checks#create")
    end

    it "routes to edit page" do
      expect(:get => "/admin/checks/1/edit").to route_to("admin/checks#edit", id: "1")
    end

    it "routes to update page" do
      expect(:put => "/admin/checks/1").to route_to("admin/checks#update", id: "1")
    end

    it "routes to show page" do
      expect(:get => "/admin/checks/1").to route_to("admin/checks#show", id: "1")
    end

    it "routes to delete page" do
      expect(:delete => "/admin/checks/1").to route_to("admin/checks#destroy", id: "1")
    end
  end
end
