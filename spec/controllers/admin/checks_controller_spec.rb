require 'rails_helper'

RSpec.describe Admin::ChecksController, type: :controller do
	login_user

	# Index page
	describe "GET #index" do
		it "assigns @checks" do
			@user.checks.build(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20)).save
			check = Check.last
			get :index
			expect(assigns(:checks)).to eq([check])
		end

		it "renders the index template" do
			get :index
			expect(response).to render_template("index")
		end

		it "does not render some other template" do
			get :index
			expect(response).to_not render_template("show")
		end
	end

	# New Page
	describe "GET #new" do
		it "assigns new check to @check" do
			expect { get :new, id: Check.create(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20)) }.to change{Check.count}.from(0).to(1)
		end

		it "renders the new template" do
			get :new
			expect(response).to render_template("new")
		end

		it "does not render some other template" do
			get :new
			expect(response).to_not render_template("show")
		end
	end

	# Create page
	describe "POST #create" do
		let(:subject) { { 'check' => {'amount' => '88.00', 'pay_to_the_order_of' => 'XYZ Company', 'date' => '2017/04/31', 'memo1' => 'For first pay' } } }
		it "creates a new check" do
			expect { post :create, subject }.to change{Check.count}.by(1)
		end
	end

	# Edit page
	describe "GET #edit" do
		it "edit a check" do
			expect { get :edit, id: Check.create(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20)) }.to change{Check.count}.by(1)
		end

		it "renders the edit template" do
			get :edit, id: Check.create(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20))
			expect(response).to render_template(:edit)
		end

		it "does not render some other template" do
			get :edit, id: Check.create(date: "2017/04/31", memo1: "For first payment", pay_to_the_order_of: "One Computer Inc.", amount: "450.00", unique_code: SecureRandom.hex(20))
			expect(response).to_not render_template("show")
		end
	end
end
