require 'rails_helper'

RSpec.describe Check, type: :model do
  subject {described_class.create}

  # Associations
  it "should belongs to User" do
		t = Check.reflect_on_association(:user)
		t.macro.should == :belongs_to
	end

  # Validations
  it "is valid with valid attributes" do
  	subject.date = "2017/04/31"
  	subject.memo1 = "For first payment"
  	subject.pay_to_the_order_of = "One Computer Inc."
  	subject.amount = "450.00"
  	subject.unique_code = SecureRandom.hex(20)
  	expect(subject).to be_valid
  end

  it "is not valid without a payto" do
  	subject.date = "2017/04/31"
  	subject.memo1 = "For first payment"
  	subject.pay_to_the_order_of = nil
  	subject.amount = "450.00"
  	subject.unique_code = SecureRandom.hex(20)
  	expect(subject).to_not be_valid
  end

  it "is not valid without a date" do
  	subject.date = nil
    subject.memo1 = "For first payment"
    subject.pay_to_the_order_of = "One Computer Inc."
    subject.amount = "450.00"
    subject.unique_code = SecureRandom.hex(20)
  	expect(subject).to_not be_valid
  end

  it "is not valid without a memo1" do
  	subject.date = "2017/04/31"
    subject.memo1 = nil
    subject.pay_to_the_order_of = "One Computer Inc."
    subject.amount = "450.00"
    subject.unique_code = SecureRandom.hex(20)
  	expect(subject).to_not be_valid
  end

  it "is not valid without an amount" do
  	subject.date = "2017/04/31"
    subject.memo1 = "For first payment"
    subject.pay_to_the_order_of = "One Computer Inc."
    subject.amount = nil
    subject.unique_code = SecureRandom.hex(20)
  	expect(subject).to_not be_valid
  end
end
