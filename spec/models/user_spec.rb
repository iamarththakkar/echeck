require 'rails_helper'

RSpec.describe User, type: :model do
	subject {described_class.new}

	# Associations
	it "should have many subscriptions" do
		t = User.reflect_on_association(:subscriptions)
		t.macro.should == :has_many
	end

	it "should have many checks" do
		t = User.reflect_on_association(:checks)
		t.macro.should == :has_many
	end
	
	# Validations
  it "is valid with valid attributes" do
  	subject.email = "user@example.com"
  	subject.password = "12345678"
  	subject.first_name = "User"
  	subject.last_name = "User"
  	subject.phone_number = "1234567890"
  	expect(subject).to be_valid
  end

  it "is not valid without an email" do
  	subject.email = nil
  	expect(subject).to_not be_valid
  end

  it "is not valid without a password" do
  	subject.password = nil
  	expect(subject).to_not be_valid
  end

  it "is not valid without a first name" do
  	subject.first_name = nil
  	expect(subject).to_not be_valid
  end

  it "is not valid without a last name" do
  	subject.last_name = nil
  	expect(subject).to_not be_valid
  end

  it "is not valid without a phone number" do
  	subject.phone_number = nil
  	expect(subject).to_not be_valid
  end

end
