Rails.application.routes.draw do
  get 'payments/check/:id', to: "payments#check", as: :pay_check
  patch 'payments/update_check', to: "payments#update_check", as: :update_check

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  root "home#index"
  get "/bettercheck", to: "home#bettercheck"
  namespace :admin do
    get "/dashboard", to: "dashboard#index", as: :dashboard
    get "/generate_check", to: "checks#generate_check", as: :generate_check, defaults: { format: 'pdf' }
    get "/change_password", to: "passwords#change_password", as: :change_password
    patch "/update_password", to: "passwords#update_password", as: :update_password
    resources :checks do
      collection do
        post "/search_check", to: "checks#search_check"
        get "/payment_link", to: "checks#payment_link"
        post "/create_payment_link", to: "checks#create_payment_link"
      end
    end
    get "/reports", to: "reports#index", as: :reports
    resources :subscriptions
  end
end
