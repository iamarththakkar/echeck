class AddAccountInfoToUser < ActiveRecord::Migration
  def change
    add_column :users, :is_account_info_provided, :boolean
  end
end
