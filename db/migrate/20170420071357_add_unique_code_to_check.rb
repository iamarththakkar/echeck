class AddUniqueCodeToCheck < ActiveRecord::Migration
  def change
    add_column :checks, :unique_code, :string
    add_index :checks, :unique_code, unique: true
  end
end
