class AddBusinessFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :business_name, :string
    add_column :users, :website, :string
    add_column :users, :street_address, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :zip, :string
    add_column :users, :industry_type, :string
    add_column :users, :is_agree, :boolean
  end
end
