class CreateChecks < ActiveRecord::Migration
  def change
    create_table :checks do |t|
      t.references :user, index: true, foreign_key: true
      t.string :account_holder_name
      t.string :street_address
      t.string :city
      t.string :state
      t.string :zip
      t.datetime :date
      t.string :check_number
      t.string :pay_to_the_order_of
      t.float :amount
      t.string :memo1
      t.string :memo2
      t.string :routing_number
      t.string :checking_account_number

      t.timestamps null: false
    end
  end
end
