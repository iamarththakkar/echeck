class AddFieldToCheck < ActiveRecord::Migration
  def change
    add_column :checks, :transaction_fee, :float
  end
end
