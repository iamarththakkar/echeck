class Check < ActiveRecord::Base
	# Filters
	after_create :generate_unique_token

  # Validations
  validates :amount, :pay_to_the_order_of, :date, :memo1, presence: true

	# Associations
  belongs_to :user

  # Constants
  MONTHS = {1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec"}

  private
  def generate_unique_token
  	token = SecureRandom.hex(20)
  	self.update_attributes(unique_code: token)
  end
end
