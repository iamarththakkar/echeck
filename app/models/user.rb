class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Attribute Accessors
  attr_accessor :current_password

  # Associations
  has_many :subscriptions, dependent: :destroy
  has_many :checks, dependent: :destroy
  has_many :companies, dependent: :destroy

  # Constants
  INDUSTRY_TYPE = ["Auto", "Collections", "Computer Support", "Finance", "Insurance", "Legal", "Manufacturing / Supplier", "Medical", "Moving / Transportation", "Professional Services", "Real Estate", "Web Development", "Other"]
end
