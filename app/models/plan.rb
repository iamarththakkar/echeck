class Plan < ActiveRecord::Base
	# Association
	has_many :subscriptions, dependent: :destroy

	# Constants
	PLANS = {"19999" => "business_premium", "24999" => "enterprise", "9999" => "business"}
end
