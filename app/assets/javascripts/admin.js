// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require admin/plugins/loaders/pace.min
//= require admin/core/libraries/jquery.min
//= require admin/core/libraries/bootstrap.min
//= require admin/plugins/loaders/blockui.min
//= require admin/plugins/ui/nicescroll.min
//= require admin/plugins/ui/drilldown
//= require admin/plugins/visualization/d3/d3.min
//= require admin/plugins/visualization/d3/d3_tooltip
//= require admin/plugins/forms/styling/switchery.min
//= require admin/plugins/forms/styling/uniform.min
//= require admin/validate.min
//= require admin/datatables-1.10.13.min
//= require admin/select2-4.0.3
//= require admin/plugins/forms/selects/bootstrap_multiselect
//= require admin/moment-2.17.1
//= require admin/bootstrap-datetimepicker-4.17.45.min
//= require unobtrusive_flash
// require pnotify
//= require pnotify.min
//= require admin/core/app
//= require admin/angular
//= require admin/mainApp
//= require admin/CheckController
PNotify.prototype.options.styling = "fontawesome";
$(document).ready(function() {
  $(window).bind('rails:flash', function(e, params) {
      if (params.type == "notice")
      {
        new PNotify({
          title: params.type,
          text: params.message,
          type: params.type,
          icon: 'fa fa-check',
          animate_speed: 200,
          delay: 5000,
          addclass: 'bg-success-400'
        });
      }
      else
      {
        new PNotify({
          title: params.type,
          text: params.message,
          type: params.type,
          icon: 'fa fa-close',
          animate_speed: 200,
          delay: 5000,
          addclass: 'bg-warning-400'
        });
      }
  });
});