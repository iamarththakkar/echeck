mainApp.controller("checkController", function($scope){
	$scope.check = {
		account_holder_name: "",
		street_address: "",
		city: "",
		state: "",
		zip: "",
		date: "",
		check_number: "",
		pay_to: "",
		amount: "",
		memo1: "",
		memo2: "",
		routing: "",
		check_account: "",

		fullAddress: function(){
			var checkObject;
			checkObject = $scope.check;
			if(checkObject.state == "")
			{
				return checkObject.city + " " + checkObject.state + " " + checkObject.zip;
			}
			else
			{
				return checkObject.city + ", " + checkObject.state + " " + checkObject.zip;
			}
		}
	};
});