class PaymentsController < ApplicationController
  def check
  	@check = Check.find_by_unique_code(params[:id])
  end

  def update_check
  	@check = Check.find(params[:check][:id])
  	@check.update(check_params)
  	if @check.save
  		redirect_to root_path, success: "You have updated check details."
  	else
  		render :check, notice: @check.errors.full_messages
  	end
  end

  private
	def check_params
    params.require(:check).permit(:account_holder_name, :street_address, :city, :state, :zip, :date, :check_number, :pay_to_the_order_of, :amount, :memo1, :memo2, :routing_number, :checking_account_number)
  end
end
