class Admin::ChecksController < Admin::BaseController
  before_filter :set_check, only: [:edit, :update, :show, :delete]

  # GET /admin/checks
  def index
    @checks = current_user.checks
  end

  # GET /admin/checks/new
  def new
    @check = Check.new
  end

  # POST /admin/checks
  def create
    @check = current_user.checks.build(check_params)
    if @check.save
      redirect_to admin_checks_path, notice: "New check has been created."
    else
      render :new, alert: @check.errors.full_messages
    end
  end

  # GET /admin/checks/:id/edit
  def edit
  end

  # PATCH /admin/checks/:id
  def update
    @check.update(check_params)
    if @check.save
      redirect_to admin_checks_path, notice: "Check has been updated."
    else
      render :edit, alert: @check.errors.full_messages
    end
  end

  # GET /admin/checks/:id
  def show
  end

  # DELETE /admin/checks/:id
  def delete
  end

  # GET /admin/checks/search_check as JS
  def search_check
    month = params[:month]
    from_date = params[:from_date]
    to_date = params[:to_date]

    if month != ""
      @checks = Check.where("extract(month from date) = ?", month)
    elsif from_date != "" and to_date != ""
      @checks = Check.where("date > ? and date < ?", from_date, to_date)
    end
  end

  # GET /admin/checks/payment_link
  def payment_link
  end

  # GET /admin/checks/create_payment_link
  def create_payment_link
    @check = current_user.checks.build(amount: params[:amount].to_f, transaction_fee: params[:transaction_fee], memo1: params[:memo], pay_to_the_order_of: params[:pay_to], date: DateTime.now)
    unless @check.save
      puts @check.errors.full_messages
    end
  end

  # GET Create PDF as PDF
  def generate_check
    @ids = eval(params[:id])
    @checks = Check.find(@ids)
    respond_to do |format|
      format.pdf do
        render :pdf => "checks", template: "admin/checks/generate_check.pdf.erb", layout: "pdf.html.erb", page_size: "Letter"
      end
    end
  end

  private
  # Strong Parameters for check
  def check_params
    params.require(:check).permit(:account_holder_name, :street_address, :city, :state, :zip, :date, :check_number, :pay_to_the_order_of, :amount, :memo1, :memo2, :routing_number, :checking_account_number)
  end

  def payment_link_params
    params.require(:check).permit(:amount, :transaction_fee, :memo1, :pay_to_the_order_of)
  end

  # Before Filter Callback method
  def set_check
    @check = Check.find(params[:id])
  end
end
