class Admin::SubscriptionsController < Admin::BaseController
	require "stripe"
	def index
		@subscriptions = current_user.subscriptions.includes(:user, :plan)
		puts "======#{@subscriptions.inspect}"
	end

	def new
		stripe_plans = Stripe::Plan.list
		@plans = {}
		stripe_plans["data"].each do |stripe_data|
			@plans.merge!("#{stripe_data["amount"]}": "#{stripe_data['name']} -- $#{stripe_data['amount'].to_i/100}/#{stripe_data['interval']}")
		end
		puts "===========#{@plans.inspect}"
	end

	def create
		@amount = params[:plan].to_i;

		token = Stripe::Token.create(
		  :card => {
		    :number => params[:card],
		    :exp_month => params[:month].to_i,
		    :exp_year => params[:year].to_i,
		    :cvc => params[:cvc]
		  },
		)

		customer = Stripe::Customer.create(
		  :email => current_user.email,
		  :source  => params[:stripeToken]
		)
		
		if customer.present?
			current_user.update_attributes(stripe_customer_token: customer.id)
			customer.sources.create(:source => token["id"])

			plan = Stripe::Plan.retrieve(Plan::PLANS["#{@amount}"])

			local_plan = Plan.where(amount: plan[:amount].to_f/100).first

			if local_plan.present?
				subscription = Stripe::Subscription.create(
					:customer => customer.id,
					:plan => plan.id
				)

				if subscription.present?
					current_user.subscriptions.build(plan_id: local_plan.id, stripe_customer_token: current_user.stripe_customer_token).save
					current_user.update_attributes(is_account_info_provided: true)
				end
			end
		end
		redirect_to admin_subscriptions_path, notice: "You've subscribed successfully."
	rescue Stripe::CardError => e
  	flash[:error] = e.message
  	puts "===========#{e.message}"
  	redirect_to admin_new_subscription_path
  end
end
