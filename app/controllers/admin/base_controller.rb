class Admin::BaseController < ApplicationController
	before_action :authenticate_user!
	before_action :check_payment
	after_filter :prepare_unobtrusive_flash
	layout "admin"

	def check_payment
		unless current_user.is_account_info_provided?
			unless params[:controller] != "subscriptions" and (params[:action] == "new" or params[:action] == "create")
				redirect_to new_admin_subscription_path, notice: "Provide your Credit Card info first."
			end
		end
	end
end