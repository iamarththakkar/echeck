class Admin::PasswordsController < Admin::BaseController
	def change_password
	end

	def update_password
		if current_user.valid_password?(params[:user][:current_password])
			if current_user.update(password: params[:user][:password], password_confirmation: params[:user][:password_confirmation])
				bypass_sign_in(current_user)
				redirect_to admin_dashboard_path, notice: "Password has been changed successfully."
			else
				redirect_to admin_dashboard_path, error: current_user.errors.full_messages
			end
		else
			redirect_to admin_dashboard_path, error: "Current Password is not matching to the data."
		end
	end
end